FROM registry.fedoraproject.org/fedora:29

RUN dnf install -y --enablerepo=updates-testing \
        git make rsync openssh-clients python3-pip \
        python3-setuptools python3-psutil python3-ruamel-yaml \
	python3-pluginbase python3-click python3-jinja2 \
	python3-protobuf python3-grpcio python3-gobject \
	ostree-libs fuse-libs lzip patch python3-requests \
	binutils file libabigail \
	flatpak flatpak-builder elfutils \
	qemu-system-x86-core qemu-system-aarch64-core qemu-system-arm-core \
	qemu-system-ppc-core \
	fakeroot \
	python3-aiohttp \
    && dnf clean all

ARG ubuntu_arch=amd64
RUN set -eu; \
    dnf install -y jq squashfs-tools; \
    [ "${ubuntu_arch}" != "ppc64le" ] && for app in core snapcraft review-tools; do \
      url="$(curl -H "X-Ubuntu-Architecture: ${ubuntu_arch}" -H 'X-Ubuntu-Series: 16' "https://api.snapcraft.io/api/v1/snaps/details/${app}" | jq '.download_url' -r)"; \
      curl -L "${url}" --output image.snap && \
      mkdir -p "/snap/${app}"; \
      unsquashfs -d "/snap/${app}/current" image.snap; \
      rm image.snap; \
    done; \
    dnf remove -y jq squashfs-tools; \
    dnf clean all

# Those paths are normally symlinks to "snap" itself which takes care
# of setting the right environment and calling the right wrapper script.
# Since we do not use snap here, instead we just put a version of the
# wrapper scripts orignally from the snaps (snapcraft and review-tools),
# and add the right environment.
COPY snapcraft-wrapper /snap/bin/snapcraft
COPY snap-review-wrapper /snap/bin/review-tools.snap-review

ENV PATH=/snap/bin:$PATH
ENV SNAP_ARCH="${ubuntu_arch}"
